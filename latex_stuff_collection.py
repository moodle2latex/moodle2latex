#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  latex_stuff_collection.py
#
#  Copyright 2015 Alberto Chiusole <bebo <AT> devzero <DOT> tk>
#

from unicode_to_latex_dicts import bad_tex_chars, unicode_to_latex


total_number_of_bullet_points_per_answer = 5

# Please change this unit accordingly to the latex units and lengths:
# https://www.sharelatex.com/learn/Inserting_Images#Reference_guide
#width_of_images = "10cm"
width_of_images = "0.5\linewidth"


def begin_document(graphic_folder_path):
    return (r"""\nonstopmode
\documentclass[a4paper,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}            % hyphenation of non-ASCII words
\usepackage{lmodern}
\usepackage[top=1in, bottom=1in, left=.5in, right=.5in]{geometry}
\usepackage{tabularx}
\usepackage{textcomp}               % needed for \\textasciimacron & co.
\usepackage{pifont}
\usepackage{wasysym}
\usepackage{amssymb}
\usepackage{textgreek}              % needed for greek letters
\DeclareUnicodeCharacter{00A0}{ }   % this because there's a character (\u8) that hides in the text.
\usepackage[space]{grffile}         % this allows images to have spaces in their filename: http://tex.stackexchange.com/a/8426
\usepackage{graphicx}               % in order to insert images.
\usepackage{fancyhdr}               % package fancyhdr and pagestyle fancy needed to set the
\pagestyle{fancy}                   %  number of the exam set.
\renewcommand{\headrulewidth}{0pt}  % this allows to remove the top header bar introduced by fancyhdr
"""
            "\\graphicspath{ {"+graphic_folder_path+"} }"
            "\n\\begin{document}")

def begin_student_section(set_number):
    return str(set_number).join((ur"\cfoot{Exam ", ur""" --- Page \thepage}

\begin{center}
    Università degli Studi di Trento\\
\end{center}
\begin{tabularx}{\textwidth}{X|X|X}
    \hline
    \textbf{Nome:} & \textbf{Cognome:} & \textbf{Matricola:}\\
    \hline
\end{tabularx}
\smallskip
"""))

end_document = r"""
\end{document}"""

begin_list = r"\begin{enumerate}"
end_list = "\\end{enumerate}\n"

# With newpage, the text is left as is; with \pagebreak, the text on the last
# page would spread out covering the whole page.
newpage = r"\newpage"

tex_black_point = r"\begingroup\fontsize{24pt}{12pt}\selectfont$\bullet$\endgroup "
tex_white_point = r"\begingroup\fontsize{24pt}{12pt}\selectfont$\circ$\endgroup "


def new_question_section(number_of_the_question):
    return "".join((ur"\section*{Domanda ", unicode(number_of_the_question), "}"))


def create_proper_bullet_point_list(text, number_item_in_list):
    """Create an item of the list with proper initial bulleted points."""

    # the slicing reverse the string and cut off the initial '0b' chars
    binary_num_of_item = bin(number_item_in_list)[:1:-1]
    binary_num_of_item = binary_num_of_item + "0"*(total_number_of_bullet_points_per_answer-len(binary_num_of_item))

    #~_item_of_the_list = [item_of_the_list.append(tex_black_point) if bit == "1" else item_of_the_list.append(tex_white_point) for bit in binary_num_of_item]
    item_of_the_list = [r"\item "]
    for bit in binary_num_of_item:
        if bit == "1":
            item_of_the_list.append(tex_black_point)
        else:
            item_of_the_list.append(tex_white_point)
    item_of_the_list.append(r"\hspace{5mm} ")
    item_of_the_list.append(text)
    return "".join(item_of_the_list)


def itemize_answers_for_a_list(question_object):
    # if not list_containg_text_answers:
    #     return "\n"

    to_return = [begin_list]
    for text_element, number in zip(question_object.answers, xrange(1, len(question_object.answers)+1)):
        to_return.append(create_proper_bullet_point_list(text_element, number))
        #~_to_return.append(new_element_of_a_list(element))
    to_return.append(end_list)
    return "\n".join(to_return)


def insert_images(list_of_images_names, function_to_convert_gifs, tempdir):
    images_in_latex = []
    for image in list_of_images_names:
        if image[-4:].lower() == ".gif":
            function_to_convert_gifs(tempdir+image)
            image = image[:-4]+".png"
        images_in_latex.extend(["\\begin{figure}[h]\n",
                                "\\includegraphics[width=", width_of_images,
                                "]{", image, "}\n",
                                "\centering\n",
                                "\\end{figure}\n"])
    return ''.join(images_in_latex)



def latex_string_replacer(string_to_be_fixed):
    """Only accept a unicode encoded string to iterate on.
    Can't use a replace method, due to the presence of sentences inside $$ that
    have to be left untouched.
    """

    # print string_to_be_fixed.encode("utf-8")

    # This list gets filled with couples of the form:
    # (position_of_char, corresponding_latex_char_to_substitute)
    # and it's then visited in the reverse order to apply the substitutions.
    _chars_to_replace = []

    first_dollar_sign, second_dollar_sign = False, False
    for pos, ch in enumerate(string_to_be_fixed): #.encode("utf-8")):
        if not first_dollar_sign and second_dollar_sign and ch == "$":
            second_dollar_sign = False
        elif not first_dollar_sign and ch == "$":
            first_dollar_sign = True
        elif first_dollar_sign and second_dollar_sign and ch == "$":
            first_dollar_sign = False
        elif first_dollar_sign and ch == "$":
            second_dollar_sign = True
        elif first_dollar_sign == second_dollar_sign == False:

            if ch in bad_tex_chars:
                # I can't replace the char now otherwise they will move the
                # positions of the following chars. I append them to a list and
                # I will visit them in the reverse order to avoid mixing.
                _chars_to_replace.append( (pos, bad_tex_chars[ch]) )

            # From the 127 to the 160th char there are some control char,
            # please refer to: http://unicode-table.com/en/#007F
            elif ord(ch) > 160:
                try:
                    # The exec stream breaks here if there's no such a key in
                    # the unicode_to_latex dict and flows to the except branch.
                    if len(unicode_to_latex[ch]) == 2:
                        if unicode_to_latex[ch][1] == "math":
                            # \(symbol\) is the latex version for the math mode
                            _chars_to_replace.append((pos, "".join(
                                    (r"\(", unicode_to_latex[ch][0], r"\)") )))
                        elif unicode_to_latex[ch][1] == "mbox":
                            _chars_to_replace.append((pos, "".join(
                                    (r"mbox{", unicode_to_latex[ch][0], r"}"))))
                    else:
                        _chars_to_replace.append((pos, unicode_to_latex[ch] ))

                except KeyError:
                    print ("* Char {} ({}) found in the text, but not present in "
                            "the unicode dictionary. Please substitute "
                            "it in the source files or add its value to the "
                            "dictionary inside unicode_to_latex_dicts.py file. "
                            "Actually ignored.".format(ch.encode("utf-8"),
                                                        hex(ord(ch)))
                          )
                    _chars_to_replace.append((pos, ""))

    for pos, latex_char in _chars_to_replace[::-1]:
        string_to_be_fixed = "".join( ( string_to_be_fixed[:pos],
                                        latex_char,
                                        string_to_be_fixed[pos+1:] )
                                    )

    # print string_to_be_fixed.encode("utf-8")
    return string_to_be_fixed

            # print ch.encode("utf-8")+"!!!"
        # print ch.encode("utf-8"), "-> first_dollar_sign:", first_dollar_sign, ", second_dollar_sign", second_dollar_sign


def _print_unicode_dict():
    for k,v in unicode_to_latex.items():
        print "{}\t{}\t- {}: {}".format(ord(k), hex(ord(k)), k.encode("utf-8"), v)


if __name__ == "__main__":
    """
    http://nedbatchelder.com/text/unipain.html
    http://latex.informatik.uni-halle.de/latex-online/latex.php
    http://detexify.kirelabs.org/classify.html
    """

    s = (u"I segnali ad alta frequenza sono trasmessi spesso mediante un "
        u"cavo coassiale, come quello mostrato nella figura.</p> Anche il cavo "
        u"della TV, ad esempio, è coassiale. Il segnale è trasportato da un "
        u"filo di raggio R<sub>1</sub> mentre il conduttore esterno di raggio"
        u"R<sub>2</sub> è messo a terra (cioè, il potenziale è a 0). Lo spazio "
        u"tra i due conduttori è riempito con un materiale isolante, e una "
        u"guaina isolante avvolge anche il conduttore esterno."
        u"Trova l'espressione per la capacità per metro di lunghezza del cavo "
        u"coassiale, assumendo che l'isolante sia aria, e calcola il valore per"
        u" R<sub>1</sub>={r1}mm e <span>R</span><sub>2</sub><span>={r2}mm."
        )

    print s.encode("utf-8")
    print latex_string_replacer(s).encode("utf-8")

    raise SystemExit
