#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  moodle2latex.py
#
#
#  ===== Copyright 2015 Alberto Chiusole <bebo <AT> devzero <DOT> tk> =====
#  I have to check every library license before deploying an official package, but meanwhile:
#
#       This work is licensed under the Creative Commons Attribution 4.0 International License.
#       To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.
#
#
#  There are some requirements to get the whole program to run properly.
#  If not specified, on a Fedora linux installation install the packages below.
#  For other distros or OSes try googling or investigating a bit.
#
#   python-BeautifulSoup    <-- for the analysis of the xml databases
#   ---texlive-babel-italian   <-- if you want to hyphenate words in pdfs
#   texlive-textgreek       <-- to print greek letters into pdf
#   (texlive-latex-extra texlive-lang-greek) on ubuntu
#   (pip install) latex     <-- to compile the tex file into pdf
#   pyparsing               <-- to parse and evaluate strings


import os
import subprocess
import tempfile
import bs4
import datetime

import database_manager as dbm
import latex_stuff_collection as lsc
import arg_in


class EnvironmentManager(object):
    """Class to contain and manage all the user input."""

    def __init__(self, namespace_obj):
        self.verbose = namespace_obj.verbose
        self.user_categories_choosen = namespace_obj.categories_names
        self.db_filenames = namespace_obj.XMLdb
        self.num_of_sets = namespace_obj.num_sets
        self.num_of_questions = namespace_obj.num_questions
        self.build_pdf = namespace_obj.pdf_production
        self.build_latex = namespace_obj.latex_production
        self.print_list = namespace_obj.print_list
        self.latex_heading = namespace_obj.latex_heading

        # Filled through parse_and_get_categories_list_from_db_filenames and
        # generate_latex_file methods below.
        self.categories_list = []
        self.latex = None


    def check_if_files_exist(self):
        for filename in self.db_filenames:
            if not os.path.isfile(filename):
                raise SystemExit("File {} not found, please "
                                "check it.".format(filename))


    def parse_and_get_categories_list_from_db_filenames(self):
        """Read every file passed as argument, extract its categories and
        append them to the class attribute categories_list.
        """

        for db_fn in self.db_filenames:
            with open(db_fn, "r+") as obj_db_fn:
                # BeautifulSoup automatically converts the text in unicode
                xml_obj = bs4.BeautifulSoup(obj_db_fn, "lxml")
            cat = dbm.exctract_and_populate_categories(xml_obj)
            self.categories_list.extend(cat)


    def check_whether_print_categories(self):
        """Only if the user request the list of available categories print
        them out: returns a boolean accordingly, in order to quit the program if
        user requested to look at the list.
        """

        if self.print_list:
            print "Categories available inside database(s)",
            if len(self.db_filenames) == 1:
                print self.db_filenames[0], ":"
            else:
                print ", ".join(self.db_filenames), ":"

            for cat in dbm.return_list_of_categories():
                print cat
            return True
        return False


    def generate_latex_file(self):
        """Given the values inserted before inside the object, produce a full
        latex file.
        """

        print "Generating the latex file.."

        self.latex = [lsc.begin_document(dbm.Question.tempdir+"/")]
        # Create a different set of questions for each student.
        for user_num in xrange(1, self.num_of_sets+1):
            self.latex.append(lsc.begin_student_section(user_num))
            # print self.categories_list
            q_list = dbm.questions_chooser_given_categories(
                                                self.user_categories_choosen,
                                                self.num_of_questions,
                                                self.categories_list)
            # Refresh moodle variables here.
            # Here I take the question and the question number.
            for (q, q_num) in zip(q_list, xrange(1, len(q_list)+1) ):
                q.refresh_question_values()
                q.shuffle_answers()
                self.latex.extend( (lsc.new_question_section(q_num),
                                    q.questiontext,
                                    lsc.insert_images(q.images.keys(),
                                                      convert_to_png,
                                                      dbm.Question.tempdir+"/"),
                                    lsc.itemize_answers_for_a_list(q) )
                                 )
            self.latex.append(lsc.newpage)

        self.latex.append(lsc.end_document)
        self.latex = "\n".join(self.latex)


    def save_latex_to_file(self, out_file_path):
        try:
            with open(out_file_path, "wb") as out:
                out.write(self.latex.encode("utf-8") )
        except IOError as e:
            raise SystemExit(" ".join(("Error during the writing of the",
                                       "latex file:", e)))


    def generate_pdf(self, out_file_path):
        temp = tempfile.NamedTemporaryFile()
        name_w_o_path = os.path.splitext(os.path.basename(temp.name))[0]

        with open(temp.name, "wb") as fo:
            fo.write(self.latex.encode("utf-8"))

        try:
            subprocess.check_output(["pdflatex", temp.name])
        except subprocess.CalledProcessError:
            pass

        extra_files = (name_w_o_path+".log", name_w_o_path+".aux")
        for f in extra_files:
            try:
                os.unlink(f)
                print "Removed extra build file:", f
            except OSError:
                pass

        try:
            os.rename(name_w_o_path+".pdf", out_file_path)
            print "Pdf built with name:", out_file_path
        except OSError as e:
            print e, "Name of file not found:", name_w_o_path+".pdf"


    def remove_blocks_of_random_numbers(self):
        """Remove from the input file all the useless blocks containing
        pre-built values that Moodle create to 'speed-up' its tasks.
        """

        for db in self.db_filenames:
            with open(db, "r+") as f_in, open(file_name, "w+") as f_out:
                file_name = "out__"+os.path.split(db)[1]
                print file_name
                # Write to the output file only the lines NOT between the
                # delimiting xml tags <dataset_items>.
                spam_flag = False
                for line in f_in:
                    # If I got to this string, I've to disable the flag.
                    if line.strip() == "</dataset_items>":
                        spam_flag = False
                    elif line.strip() == "<dataset_items>" or spam_flag:
                        spam_flag = True
                    else:
                        f_out.write(line)
        return


def convert_to_png(image_file_name):
    """If no path is included in the input image file name, the image must be
    placed in the current directory the script run from.
    """

    image_without_ext = os.path.splitext(image_file_name)[0]
    # No return status check.
    with open(os.devnull, "a") as dev_null:
        subprocess.call(["convert", image_file_name, image_without_ext+".png"],
                        stderr=dev_null
                       )


def main():
    env = EnvironmentManager(arg_in.parse_cli_args())

    env.check_if_files_exist()
    #env.remove_blocks_of_random_numbers()
    #return
    env.parse_and_get_categories_list_from_db_filenames()

    # If user choose to see the list of categories, print it out and exit.
    if env.check_whether_print_categories():
        raise SystemExit

    current_time = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    output_file_name = "out_"+current_time

    env.generate_latex_file()

    env.save_latex_to_file(output_file_name+".tex")

    env.generate_pdf(output_file_name+".pdf")

    return



if __name__ == "__main__":
    """
    import sys; sys.argv.extend([ #'-v',
                    '-nq', '2',
                    '-ns', '4',
                    'out__grande.xml',
                    'out__piccolo.xml',
                    '-c', 'Elettromagnetismo/Elettrostatica applicata/Elettrostatica senza dipoli e senza materiali/Condensatori (difficili)',
                    'Elettromagnetismo/Elettrostatica applicata/Elettrostatica senza dipoli e senza materiali/Elettrostatica geometrica nel vuoto (difficili)'
                    ])
    """

    main()



"""
TODO:
*   quit the program if --no-latex and no --pdf options are set.
*   ask if a Moodle variable could also be made with underscores "{var1_foo}".
*   Set verbosity as an optional parameter in each function (or as a global var).
TODO:   check that two questions coming from different dbs cannot be added to
        the same category name [override the equality or the 'in' method of
        Question class to check if a question with the same characteristics is
        in the category class yet].
* __FEATURE__   in the moodle_variable, there is the possibility to create some
                random values using a logarithm scale, so values near the
                minimum are more probable to come out.

RELEASE NOTE:   two db with categories that have the same name, will join
                together under the same category their quetions.
"""

