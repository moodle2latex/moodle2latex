#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  moodle_parser.py
#
#  Copyright 2015 Alberto Chiusole <bebo <AT> devzero <DOT> tk>
#

import pyparsing as pp
import operator
from collections import deque
import math
import random
import sys

number_digits_precision = "10"

moodle_funcs_web = ("https://docs.moodle.org/22/en/Calculated_question_"
                    "type#Available_functions")


# If you edit this char below, don't use an arithmetic symbol or a function name
# like the ones in add_ops, mul_ops or moodle_functions dictionaries.
func_args_separator = "."

add_ops = { "+": (operator.add, 2),
            "-": (operator.sub, 2)
          }

mul_ops = { "*": (operator.mul, 2),
            "/": (operator.truediv, 2)
          }

# Just an extended dict containing the above two arithmetic dicts.
arith_ops_dict = {}
arith_ops_dict.update(add_ops)
arith_ops_dict.update(mul_ops)


# Dict containing moodle functions and corrispective python functions:
# https://docs.moodle.org/22/en/Calculated_question_type#Available_functions
#
# The "pi()" function is managed internally at the lexer (see Syntax function)
# because it's a function without args and it would have taken more work time to
# manage it like a normal function.
moodle_functions = {"abs": (abs, 1),
                    "acos": (math.acos, 1),
                    "acosh": (math.acosh, 1),
                    "asin": (math.asin, 1),
                    "asinh": (math.asinh, 1),
                    "atan": (math.atan, 1),
                    "atan2": (math.atan2, 2),
                    "atanh": (math.atanh, 1),
                    "ceil": (math.ceil, 1),
                    "cos" : (math.cos, 1),
                    "cosh" : (math.cosh, 1),
                    "decbin": (bin, 1),
                    "decoct": (oct, 1),
                    "deg2rad": (math.radians, 1),
                    "exp": (math.exp, 1),
                    "expm1": (lambda x: math.exp(x)-1, 1),
                    "floor": (math.floor, 1),
                    "fmod": (math.fmod, 2),
                    "is_finite": (lambda x: not math.isinf, 1),
                    "is_infinite": (math.isinf, 1),
                    "is_nan": (math.isnan, 1),
                    "log10": (math.log10, 1),
                    "log": (math.log, 1),
                    "log1p": (math.log1p, 1),
                    "max": (max, -1),
                    "min": (min, -1),
                    "octdec": (lambda x: int(x, 8), 1),
                    "pow": (math.pow, 2),
                    "rad2deg": (math.degrees, 1),
                    # TODO: check if the rand func in moodle works exactly like this
                    "rand": (lambda x: random.randint(-sys.maxint-1,
                                        sys.maxint), 1),
                    "round": (round, 1),
                    "sin": (math.sin, 1),
                    "sinh": (math.sinh, 1),
                    "sqrt": (math.sqrt, 1),
                    "tan": (math.tan, 1),
                    "tanh": (math.tanh, 1)
                    }


class Token(object):
    """This class manages arithmetic operators like + - * / and parenthesis,
    giving them a precedence (an order of relevance between operators) needed
    in the shunting yard algorithm.
    """

    def __init__(self, symbol_char):
        # The order (or "importance") means simply the precedence an operator
        # has among others.
        # The default for non-add or non-mul symbol (e.g. parenthesis) is 0
        self.importance = 0

        # Sometimes pyparsing gives out an element made like a list ["elem"]
        if isinstance(symbol_char, pp.ParseResults):
            assert len(symbol_char) == 1
            self.symbol_char = symbol_char[0]
        elif isinstance(symbol_char, str):
            self.symbol_char = symbol_char
        else:
            raise Exception("There is something strange.. in the "
                            "neighbourhood..")

        # Moodle implement the pow function in substitution of the ^ power
        # symbol, so we don't need to manage right-associativity.
        if self.symbol_char in add_ops:
            self.importance = 1
        elif self.symbol_char in mul_ops:
            self.importance = 2

    def __hash__(self):
        return hash(self.symbol_char)

    def __eq__(self, external_string):
        return self.symbol_char == external_string

    def __ne__(self, external_string):
        return not self == external_string

    # No exception controls on this class.
    def __lt__(self, second_arith_operator):
        return self.importance < second_arith_operator.importance

    def __ge__(self, second_arith_operator):
        return not self < second_arith_operator

    def __gt__(self, second_arith_operator):
        return self.importance > second_arith_operator.importance

    def __le__(self, second_arith_operator):
        return not self > second_arith_operator

    def __str__(self):
        return self.symbol_char

    def __repr__(self):
        return "'{0}'".format(self.symbol_char)


def Syntax():
    """This function is only a wrapper for managing PyParsing grammar.
    Simply assign its return to a variable and call its parseString method on
    a string.

    The EBNF grammar is located at "grammar.ebnf".
    """

    digits = pp.Word(pp.nums).setName("digits")
    plus_or_minus = pp.oneOf("+ -").setParseAction(lambda op: Token(op)).setName("+or-")
    mul_or_div = pp.oneOf("* /").setParseAction(lambda op: Token(op)).setName("*or/")
    point = pp.Word(".").setName("point")
    left_par = pp.Literal("(").setParseAction(lambda op: Token(op)).setName("left parenthesis")
    right_par = pp.Literal(")").setParseAction(lambda op: Token(op)).setName("right parenthesis")
    variable_name = pp.Word(pp.alphas, pp.alphas+pp.nums).setName("variable identificator")


    unsigned_int = digits
    signed_int = pp.Combine(plus_or_minus + unsigned_int).setName("signed int")
    opt_signed_int = pp.Combine(pp.Optional(plus_or_minus) +
                    unsigned_int).setParseAction(lambda el: int(el[0])).setName("optionally signed int")
    float_num = pp.Combine(
                    pp.Optional(plus_or_minus) + ( unsigned_int + point +
                    pp.Optional(unsigned_int) ) ^ (point + unsigned_int) +
                    pp.Optional(pp.CaselessLiteral("e") + opt_signed_int)
                ).setParseAction(lambda el: float(el[0])).setName("float number")
    moodle_var = pp.Combine(
                    pp.Optional(plus_or_minus) + "{" + variable_name + "}"
                 ).setName("moodle var")
    pi = pp.Combine(pp.Optional(plus_or_minus) +
                    pp.Literal("pi()").setParseAction(lambda el: math.pi)
                ).setParseAction(lambda el: float(el[0])).setName("greek pi")

    real_num =  (float_num ^ opt_signed_int ^ moodle_var ^ pi).setName("any num")

    add_op = pp.Forward()
    mul_op = pp.Forward()
    expr = pp.Forward()

    function = ( pp.Optional(plus_or_minus) +
                variable_name + left_par + add_op +
                pp.ZeroOrMore("," + add_op) + right_par ).setName("function")

    add_op << ( mul_op + pp.ZeroOrMore( plus_or_minus + mul_op ) ).setName("add operator")
    mul_op << ( expr + pp.ZeroOrMore( mul_or_div + expr) ).setName("mul operator")
    expr << ( ( pp.Optional(plus_or_minus) + left_par + add_op + right_par) ^
                function ^ real_num
            ).setName("atomic expression")

    return add_op #.setDebug()


# Here I instantiate a single pyparsing grammar object to execute all the
# parsing operations on during the execution of the whole program.
# This reduce the time each evaluation takes.
_moodle_grammar = Syntax()


def shunting_yard(tokens_list):
    """Given a list of numbers and Token objects, it returns a postfix notation
     ordered queue, made of a deque data type object.

    https://en.wikipedia.org/wiki/Shunting-yard_algorithm
    http://www.reedbeta.com/blog/2011/12/11/the-shunting-yard-algorithm/
    """

    # Using a list for the output queue isn't so efficient than using the deque
    # class. Look at the official doc:
    # https://docs.python.org/tutorial/datastructures.html#using-lists-as-queues
    output_queue = deque()
    operator_stack = []
    inverted_tokens_list = tokens_list[::-1]

    # The list below holds the name of functions that accept a potentially
    # infinite number of arguments.
    funcs_inf_nargs = [f_nargs
                       for f_nargs in ( k if v[1] == -1 else None
                                        for k, v in moodle_functions.items()
                                       )
                       if f_nargs is not None
                      ]

    # This internal function lightens the code below when I have to empty
    # the operator_stack.
    def empty_stack_until_left_paren(delete_left_paren=True):
        while operator_stack:
            if operator_stack[-1] != "(":
                output_queue.append(operator_stack.pop())
            else:
                if delete_left_paren:
                    operator_stack.pop()
                return
        # If the stack gets empty without finding a left parenthesis, it means
        # the expression isn't correct.
        raise Exception("Missing left parenthesis or function name "
                                "before arguments.")

    while inverted_tokens_list:
        element = inverted_tokens_list.pop()

        if isinstance(element, (int, float)):
            output_queue.append(element)

        elif element in moodle_functions:
            # If the function can accept a potentially infinite number of args..
            if element in funcs_inf_nargs:
                # ..add a separator between those arguments and the values
                # outside the function.
                output_queue.append(func_args_separator)
            operator_stack.append(element)

        elif element == ",":
            empty_stack_until_left_paren(delete_left_paren=False)

        elif element == "(":
            operator_stack.append(element)

        elif element == ")":
            empty_stack_until_left_paren()
            # Move function name from stack to output; the RPN_calc will manage
            # and use its fields correctly.
            if operator_stack and operator_stack[-1] in moodle_functions:
                output_queue.append(operator_stack.pop())

        # Parenthesis are captured by conditions above, so there's no way to
        # have element=="(" (or ")") at this point.
        elif isinstance(element, Token):
            # Until there are operators on the stack that have greater
            # precedence and the stack is not empty:
            while operator_stack:
                #print element, " *** ", operator_stack[-1]
                if operator_stack[-1] in moodle_functions:
                    output_queue.append(operator_stack.pop())
                # Since Moodle doesn't have right-associative operators (e.g. ^)
                # I only need to check that the top (last) operator has smaller
                # precedence order.
                elif element<=operator_stack[-1]:
                    output_queue.append(operator_stack.pop())
                else:
                    break

                #assert operator_stack[-1] != "(","I've found a left par: (. Error."
            operator_stack.append(element)

        else:
            raise Exception( element.join(("The character or function ",
                                        " analyzed can't be calculated."))
                            )

    # Until it's not empty.. empty it.
    while operator_stack:
        # The str method below is required to check whether the operator is a
        # parenthesis or not.
        if str(operator_stack[-1]) in "()":
            raise Exception("Parenthesis mismatch.")
        output_queue.append(operator_stack.pop())

    #print output_queue, "=>",
    return output_queue


def rpn_calculator(list_of_tokens):
    """This function will calculate the expression inside list_of_tokens input,
    which is given in Reverse Polish Notation:
    (e.g. "3+(1-5)/pi()" becomes: "3 1 5 - pi() / +" )

    https://en.wikipedia.org/wiki/Reverse_Polish_notation
    """

    rpn_stack = deque()
    #print "initial list of tokens:", list_of_tokens
    while list_of_tokens:
        element = list_of_tokens.popleft()

        if isinstance(element, (int, float)) or element == func_args_separator:
            rpn_stack.append(element)
        else:
            # .. else it's an operator (a function or an arithmetic op), and
            # have to check whether there are the proper number of values on
            # the stack to be evaluated.
            if element in moodle_functions:
                number_args = moodle_functions[element][1]

                if number_args != -1 and len(rpn_stack) < number_args:
                    raise Exception("In the input string there are fewer "
                                    "numeric elements than needed. Check it.")
                # A number of args == -1 means the function can take a
                # potentially infinite number of args from the stack
                # (e.g. min, max functions). The calculator stops when it
                # encounters the func_args_separator -> "."
                if number_args == -1 and rpn_stack:
                    arguments_to_function = []
                    for _ in xrange(len(rpn_stack)):
                        if rpn_stack[-1] != func_args_separator:
                            arguments_to_function.append(rpn_stack.pop())
                        elif rpn_stack[-1] == func_args_separator:
                            # Discard the separator and kill the loop
                            rpn_stack.pop()
                            break
                        else:
                            assert False, "This string shouldn't never arise."
                elif number_args != -1:
                    arguments_to_function = [rpn_stack.pop() for _ in
                                                xrange(number_args)][::-1]
                result = moodle_functions[element][0](*arguments_to_function)
                rpn_stack.append(result)
            elif element in arith_ops_dict:
                number_args = arith_ops_dict[element][1]

                arguments_to_function = [rpn_stack.pop() for _ in
                                         xrange(number_args)][::-1]
                result = arith_ops_dict[element][0](*arguments_to_function)
                rpn_stack.append(result)
            else:
                raise Exception("There shouldn't exist element variable that "
                                "are neither a moodle function or an "
                                "arithmetic operator.\nFor a list of moodle "
                                "functions please look at: "+moodle_funcs_web)

    # If there's a different number of values left on the stack, it means that
    # there are missing operations or functions to execute. User error.
    if len(rpn_stack) != 1:
        raise Exception("There is an error with the input given to the "
                        "RPN calculator. Please check it.")
    return rpn_stack[0]


def evaluate_expression(arith_string):
    """Instance a pyparsing parser obj and evaluate the expression with it."""

    # Fixing + or - first char: this way is cheaper than modifying the parser.
    if arith_string[0] in "+-":
        arith_string = "0"+arith_string

    # Maybe creating a pyparsing istance each time an expression has to be
    # calculated isn't really efficient, and putting it as a "module-variable"
    # could avoid this overload.
    tokens_list = _moodle_grammar.parseString(arith_string)
    rpn_list = shunting_yard(tokens_list)
    result = rpn_calculator(rpn_list)
    return number_digits_precision.join(("{:.", "f}")).format(result)


if __name__ == "__main__":
    for i in [
            "1.1/abs(5.0-6.1)-1",
            "(1.1/abs(5.0-6.1))-1",
            "(1.1/abs(5.0-6.1))-1+1",
            "((1.1/abs(5.0-6.1))-1)",
            "+pi()",
            "-pow(2,3)",
            "pow(235,5.0-6+0.5*((5.0-6.1)/abs(5.0-6.1)-1))*(sqrt(pow(0.3/2,2)+0.3*39.3/pi()/8.85/4*100)-0.3)"
            "3.5*1000/(2*9*log(3.1/0.3))",
            "10/(2*9*log(3.1/0.3))+pow(2.5,3.4+0.5*((30.4-1)/abs(30.4-1)-1))*sqrt(155.5*(sqrt(955.6+1500.6*1500.6)-1500.6)/10.7/8.854)"
            ]:
        print i, "=> ", evaluate_expression(i)
