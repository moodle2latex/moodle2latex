#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  arg_in.py
#
#  Copyright 2015 Alberto Chiusole <bebo <AT> devzero <DOT> tk>
#

# https://docs.python.org/2/library/argparse.html
# https://docs.python.org/2/howto/argparse.html
import argparse

prog_name = "Moodle2Latex"


class SmartFormatter(argparse.HelpFormatter):
    """A modifier for argparse that allows to split a string on multiple lines,
    simply by preceding it with a "R|".

    Taken from SO: http://stackoverflow.com/a/22157136
    """

    def _split_lines(self, text, width):
        # this is the RawTextHelpFormatter._split_lines
        if text.startswith('R|'):
            return text[2:].splitlines()
        return argparse.HelpFormatter._split_lines(self, text, width)


def check_negative(value):
    """Integer passing check is done by default thanks to default=NUMBER to
    add_argument method."""
    if int(value) <= 0:
        raise SystemExit("Input error on command line: numeric value can't be "
                        "below 1.")
    return int(value)


def perform_controls_on_input(args, parser):
    """Perform some controls on the input, that couldn't be made directly with
    the argument parser.
    """

    def exit_with_message(msg):
        parser.print_usage()
        raise SystemExit(msg)

    # If verbosity is set, state the args passed to the script.
    if args.verbose:
        print "Arguments passed to the program:", \
            "\nargs.verbose =>", args.verbose, \
            "\nargs.XMLdb =>", " ".join(args.XMLdb), \
            "\nargs.categories_names =>", " ".join(args.categories_names), \
            "\nargs.num_sets =>", args.num_sets, \
            "\nargs.num_questions =>", args.num_questions, \
            "\nargs.pdf_production =>", args.pdf_production, \
            "\nargs.latex_production =>", args.latex_production, \
            "\nargs.print_list =>", args.print_list, \
            "\nargs.latex_heading =>", args.latex_heading, "\n"


    if args.print_list and (args.num_sets != -1 or args.num_questions != -1):
        exit_with_message(prog_name.join(("\n",
                            ": error: `list` argument not allowed with "
                            "`number_of_sets` or `number_of_questions` "
                            "argument(s) enabled at the same time."))
                        )
    elif not args.print_list and args.num_sets == -1:
        exit_with_message(prog_name.join(("\n",
                            ": error: a number of sets "
                            "to be generated is expected."))
                        )
    elif not args.print_list and args.num_questions == -1:
        exit_with_message(prog_name.join(("\n",
                            ": error: a number of questions "
                            "to be generated is expected."))
                        )


def parse_cli_args():
    """Analyzes the input (which by default it's sys.argv) searching for the
    moodle db and other details, and returns a dict containing all the
    values found.
    """
    parser = argparse.ArgumentParser(prog=prog_name,
                        description="""Extract questions from a 'Calculated
                        Questions' Moodle XML database and outputs them all as
                        a latex or pdf file ready for an exam.""",
                        formatter_class=SmartFormatter)
    parser.add_argument("XMLdb", nargs="+",
                        help="insert (at least one) path to a Moodle XML "
                            "database")
    parser.add_argument("-l", "--list",
                        action="store_true",
                        dest="print_list",
                        help="print the list of all the categories in all the "
                            "XML databases provided")
    parser.add_argument("-ns", "--number-sets",
                        default=-1,
                        # required=True,
                        type=check_negative,
                        metavar="NUM",
                        dest="num_sets",
                        help="number of randomly different sets of questions "
                            "(each set of questions is a complete exam for a"
                            " student)")
    parser.add_argument("-nq", "--number-questions",
                        default=-1,
                        # required=True,
                        type=check_negative,
                        metavar="NUM",
                        dest="num_questions",
                        help="number of questions to be generated for each "
                            "set of questions; they must be equal or greather "
                            "than the number of categories inserted")
    parser.add_argument("-c", "--category",
                        metavar="CAT",
                        default="*",
                        dest="categories_names",
                        nargs="*",
                        help="R|from which categories have to choose the "
                            "questions:\nwrite multiple categories one after the"
                            " other \nPS: use --list to display all the "
                            "categories available")
    parser.add_argument("-nopdf", "--disable-pdf",
                        action="store_false",
                        dest="pdf_production",
                        help="disable the production of the pdf file")
    parser.add_argument("-notex", "--disable-latex",
                        action="store_false",
                        dest="latex_production",
                        help="disable the production of the latex file")
    parser.add_argument("-he", "--heading",
                        default=' '.join((u"Università degli studi di Trento",
                                            "-- \\today")),
                        dest="latex_heading",
                        help="an heading in latex format to be inserted on the"
                            " top of each set of questions")
    parser.add_argument("-v", "--verbose",
                        action="store_true",
                        dest="verbose",
                        help="increase verbosity of the whole program")

    args = parser.parse_args()
    perform_controls_on_input(args, parser)

    return args



if __name__ == '__main__':
    import sys; sys.argv.extend(['-l',
                    #'-nq', '2',
                    #'-ns', '4',
                    'out__grande.xml',
                    #'out__piccolo.xml',
                    '-c', 'Elettromagnetismo/Elettrostatica applicata/Elettrostatica senza dipoli e senza materiali/Condensatori (difficili)',
                    'Elettromagnetismo/Elettrostatica applicata/Elettrostatica senza dipoli e senza materiali/Elettrostatica geometrica nel vuoto (difficili)'
                    ])
    #print sys.argv
    parse_cli_args()