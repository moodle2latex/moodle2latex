# Moodle2LaTeX - a Moodle databases to LaTeX converter

Moodle2LaTeX is a desktop converter that takes one (or more) XML database(s) exported from [moodle](https://moodle.org/) [QuizTest](https://docs.moodle.org/en/Quiz_activity), and converts them to a LaTeX and/or pdf file.


The purpose of moodle2latex is to create partial exam tests ("provette" in Italian universities) combined with [ZipGrade](https://www.zipgrade.com), a software for quickly checking students' results through a smartphone.

The final output should look like [this](http://ibin.co/2I9QFYlMgsQb).
Presently, the software creates a file that looks like [this](http://ibin.co/2VW7NI8H0fcr).


# Required packages:

In order to work, this software needs some packages.

On a **Fedora** Linux distribution (add a *dnf install* before each row):

* python2 python-beautifulsoup4
* ImageMagick
* texlive-latex texlive-latex-fonts

On an **Ubuntu** Linux distribution (add a *apt-get install* before each row):

* python2 python-bs4
* Imagemagick
* texlive-latex-base texlive-latex-extra


# Source code
The source code is hosted and managed on [bitbucket](https://bitbucket.org/moodle2latex/moodle2latex).


Feel free to contact me for any information (you can find my email contact on my [website](http://devzero.tk)).