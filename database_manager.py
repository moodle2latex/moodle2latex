#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  database_manager.py
#
#  Copyright 2015 Alberto Chiusole <bebo <AT> devzero <DOT> tk>
#

import re
import random
import os.path
import tempfile

import latex_stuff_collection as lsc
import moodle_parser


class Category(object):
    """Manages all the category attributes.

    It needs a BeautifulSoap Tag object containing the whole category tree.
    """

    # Every Category istance will add its name to this list, so it can be
    # easily retrieved later from the function return_list_of_categories.
    # Each category of any moodle db passed to the program will add itself to
    # this list.
    complete_cat_list = []

    def __init__(self, cat_name, list_of_question_objects):
        if cat_name.strip().startswith(ur"$course$/"):
            cat_name = cat_name.strip()[9:]
        else:
            cat_name = cat_name.strip()
        self.category_name = cat_name

        if self.category_name not in self.__class__.complete_cat_list:
            self.__class__.complete_cat_list.append(cat_name)

        self.questions = [Question(obj) for obj in list_of_question_objects]

    def __str__(self):
        return self.category_name

    def __repr__(self):
        return repr(str(self))

    def __rsub__(self, other):
        try:
            return [string for string in other if string != self ]
        except TypeError:
            raise TypeError("Operation not implemented inside rsub method of"
                            "Category class.")


class Question(object):
    """A container for all the attributes of a question in the database.

    This class accept one BeautifulSoap Tag object containing the whole
    question tags.
    """

    # I store the variable containing the path to a temp directory here, in
    # order to allow each Question object access it.
    tempdir = tempfile.mkdtemp()


    def __init__(self, bs4_object):
        self.numeric_vars = [MoodleVariable(var) for var in
                                    bs4_object.find_all("dataset_definition")]
        self.name = bs4_object.select("name")[0].get_text().strip()

        self.orig_questiontext = self._clean_moodle_text(bs4_object
                                .questiontext.select("text")[0].get_text())
        self.orig_generalfeedback = self._clean_moodle_text(bs4_object
                                .generalfeedback.select("text")[0].get_text())

        #self.images = bs4_object.questiontext.find_all("file", encoding="base64")
        self.images = {i.attrs["name"] : i.text for i in
                        bs4_object.questiontext.find_all("file", encoding="base64") }

        # The following two vars will contain the final text of the question.
        # In order to have different question texts for each set of questions
        # (=student), I regenerate the question with random values each time
        # with refresh_question_values method.
        self.questiontext = ""
        self.generalfeedback = ""

        self.already_escaped_for_latex = False

        self.original_answers = [var.select("text")[0].text for var in
                                            bs4_object.find_all("answer")]
        self.answers = []

        if len(self.original_answers) > 2**lsc.total_number_of_bullet_points_per_answer:
            raise Exception("There are too much answers ( >{} ) available for "
                        "the question with text:\n\n{}\n\nPlease check the "
                        "question in the database and restart "
                        "the program.".format
                        (2**lsc.total_number_of_bullet_points_per_answer,
                        self.questiontext.encode("utf-8")))


    def refresh_variables_value(self):
        for var in self.numeric_vars:
            var.refresh_value()

    def refresh_answers_value(self):
        self.answers[:] = []
        for ans in self.original_answers:
            a = lsc.latex_string_replacer(calculate_moodle_exprs(ans,
                                                            self.numeric_vars))
            self.answers.append(a)


    def refresh_texts_value(self):
        if not self.already_escaped_for_latex:
            self.questiontext = lsc.latex_string_replacer(
                                    calculate_moodle_exprs(
                                        self.orig_questiontext,
                                        self.numeric_vars)
                                )
            self.generalfeedback = lsc.latex_string_replacer(
                                    calculate_moodle_exprs(
                                        self.orig_generalfeedback,
                                        self.numeric_vars)
                                )
            self.already_escaped_for_latex = True

    def shuffle_answers(self):
        random.shuffle(self.answers)

    def create_images(self):
        """TODO: Need to document the image management better."""

        for image_name in self.images.keys():
            image_path = "/".join((self.__class__.tempdir, image_name))
            if not os.path.isfile(image_path):
                with open(image_path, "w+") as i:
                    i.write(self.images[image_name].decode('base64'))


    def refresh_question_values(self):
        self.refresh_variables_value()
        self.refresh_answers_value()
        self.refresh_texts_value()
        self.create_images()

    def _clean_moodle_text(self, text):
        """Remove the last three digit if they are ']]>'."""
        if text[-3:] == "]]>":
            return text[:-3]
        return text

    def __eq__(self, other):
        """Two questions are the same if they are equal for each attribute."""
        try:
            # The two lists of moodle vars are the same if every var is inside
            # the opposite list.
            for var_self in self.numeric_vars:
                if var_self not in other.numeric_vars:
                    return False
            for var_other in other.numeric_vars:
                if var_other not in self.numeric_vars:
                    return False
            # Don't check the equality of the images for speed.
            if (self.name != other.name or
                    self.orig_questiontext != other.orig_questiontext or
                    self.orig_generalfeedback != other.orig_generalfeedback):
                return False
            return True
        except AttributeError:
            return False

    def __str__(self):
        return ':: '.join((self.name, self.orig_questiontext[:50])).encode('utf-8')

    def __repr__(self):
        # In this way, unicode chars are returned correctly.
        return str(self).join(("'", "'"))


# check whether multichoice questions need a different approach!
class MoodleVariable(object):
    """A class that manages a moodle numerical variable.

    Given one BeautifulSoap Tag object (the discendants of
    <dataset_definitions>) it crawls for the minimum and maximum, for the number
    of decimals and eventually creates a random number given these parameters.
    """

    def __init__(self, bs4_object):
        if bs4_object.select("name") and bs4_object.select("type"):
            # Due to the superposition of name tag in the xml db and of internal
            # var of the bs class 'name', I had to use '.select' to bypass it.
            self.name_of_var = bs4_object.select("name")[0].get_text().strip()
            self.with_curly_braces = "".join(("{",self.name_of_var,"}"))
            self.decimals = int(bs4_object.decimals.get_text())
            self.minimum = float(bs4_object.minimum.get_text())
            self.maximum = float(bs4_object.maximum.get_text())
            self.value = None
            if self.decimals < 0:
                raise Exception("Error: negative number of decimals within "
                                "Moodle variable {}.".format(self.name_of_var))


    def refresh_value(self):
        self.value = round(random.uniform(self.minimum, self.maximum),
                                                                self.decimals)
        #if self.decimals == 0:
            #self.value = int(self.value)

    def __eq__(self, other):
        # Use duck typing on a variable of the same class.
        try:
            return (self.name_of_var == other.name_of_var and
                    self.decimals == other.decimals and
                    self.minimum == other.minimum and
                    self.maximum == other.maximum)
        except AttributeError:
            return self.name_of_var == other



def substitute_moodle_vars(text, list_of_moodle_vars):
    """This function seek in the text var all the moodle variables of this form
    '{var1}' and substitute them with the value found in the list argument.
    """

    simple_vars = re.findall(r"{(\w[\d\w]*)}", text)
    for simple in simple_vars:
        for var in list_of_moodle_vars:
            if simple == var.name_of_var:
                text = text.replace(var.with_curly_braces, str(var.value))
                break
    return text


def calculate_moodle_exprs(text_to_parse, list_of_moodle_vars=None):
    """This function find all the expressions of this form '{=....}', evaluate
    them through the moodle_parser module and return the whole text var.
    """

    #print "### initial answer text:", text_to_parse

    if list_of_moodle_vars:
        #print "*** found list of vars"
        text_to_parse = substitute_moodle_vars(text_to_parse,
                                               list_of_moodle_vars)

    #print text_to_parse,
    # Search for every formula `{=...}` in the text and calculate it.
    expressions = re.findall(r"({=.*?})", text_to_parse)
    for expr in expressions:
        num_result = moodle_parser.evaluate_expression(expr[2:-1])
        text_to_parse = text_to_parse.replace(expr, str(num_result))
    #print "### final answer text:", text_to_parse
    #print "*************", text_to_parse
    return text_to_parse



def exctract_and_populate_categories(bs4_object):
    """Return a list of category objects found in the input bs4 object given."""

    questions_per_category = []
    category_list = []

    for question_tag in bs4_object.select("quiz > question"):

        # If a category sub-field exists, I need to register the name.
        if question_tag.category:

            # If the list containing the questions is full, we have to create
            # a category object with the name of the last category seen and the
            # list of all questions collected before.
            if questions_per_category:
                # Real istantiation of the class
                category_list.append(Category(cat_name, questions_per_category))
                questions_per_category[:] = []
            cat_name = question_tag.category.text
        else:
            # Add questions belonging to the same category to a list
            questions_per_category.append(question_tag)

    if questions_per_category:
        category_list.append(Category(cat_name, questions_per_category))

    return category_list


def questions_extractor(category_path_string, category_list):
    """Given a list of categories and a category string name, return the list
    of questions that starts with that string, shuffled.
    The function will match both an entire category name as well as a partial
    category name  (e.g. 'MyCategories/foo' will match 'MyCategories/foo_3.14'
    and 'MyCategories/foo/bar' )."""

    questions_list = []

    # If '*' is passed as argument, it means user expects questions from
    # any category.
    if category_path_string == "*":
        for c in category_list:
            questions_list.extend(c.questions)
        return questions_list

    for cat in category_list:
        if cat.category_name.startswith(category_path_string):
            questions_list.extend(cat.questions)
    return questions_list


def return_list_of_categories():
    return Category.complete_cat_list


def questions_chooser_given_categories(list_user_categories_names,
                                       num_of_questions,
                                       list_categories_objects):
    questions_to_produce = []

    # print list_categories_objects

    # For each category passed to function, randomly extract
    # NQuestions/NCategory (floored to smallest integer) questions from them.
    min_questions_per_category = num_of_questions / len(list_user_categories_names)

    for user_category in list_user_categories_names:
        questions_to_choose_from = questions_extractor(user_category,
                                                        list_categories_objects)
        random.shuffle(questions_to_choose_from)
        for _ in xrange(min_questions_per_category):
            questions_to_produce.append(questions_to_choose_from.pop())

    # Get the number of questions not sequentially extracted above, and
    # randomly choose a category for each one of them.
    num_remaining_questions = num_of_questions % len(list_user_categories_names)
    while num_remaining_questions > 0:
        random_category = random.choice(list_categories_objects)

        # Check whether the category choosen is "empty".
        for q in random_category.questions:
            if q not in questions_to_produce:
                break
        else:
            # It means that all the questions of the category chosen
            # are already inside the final list of questions.
            # Remove the category from the future choices.
            list_categories_objects = list_categories_objects - random_category
            continue

        # And eventually choose a question from the randomly extracted category.
        random_question = random.choice(random_category.questions)

        if random_question not in questions_to_produce:
            questions_to_produce.append(random_question)
            num_remaining_questions -= 1

    return questions_to_produce



if __name__ == "__main__":
    print ("This module has not to be executed directly, since it just contains"
          " functions and classes related to the manage of the Moodle XML db.")

    ld = [
            {'name_of_var': 'q1', 'maximum': 25450.0, 'value': 1555550.5, 'minimum': 5.0, 'with_curly_braces': '{q1}', 'decimals': 1},
            {'name_of_var': 'sigma', 'maximum': 20.0, 'value': 1556.8, 'minimum': 5.0, 'with_curly_braces': '{sigma}', 'decimals': 1},
            {'name_of_var': 'a1', 'maximum': 20.0, 'value': 955.6, 'minimum': 5.0, 'with_curly_braces': '{a1}', 'decimals': 1},
            {'name_of_var': 'b1', 'maximum': 5555.0, 'value': 1500.6, 'minimum': 1.0, 'with_curly_braces': '{b1}', 'decimals': 1},
            {'name_of_var': 'm1', 'maximum': 2.0, 'value': 10.7, 'minimum': 1.0, 'with_curly_braces': '{m1}', 'decimals': 1},
            {'name_of_var': 'n1', 'maximum': 7.0, 'value': 3.4, 'minimum': 0.0, 'with_curly_braces': '{n1}', 'decimals': 0}
         ]

            #self.name_of_var=0
            #self.maximum=0
            #self.minimum=0
            #self.value=0
            #self.with_curly_braces=0
            #self.decimals=0
    class O(object):
        def __init__(self):
            self.name_of_var,self.maximum,self.minimum,self.value,self.with_curly_braces,self.decimals=None,None,None,None,None,None

        def __str__(self):
            return '::'.join((self.name_of_var,str(self.maximum),str(self.value),str(self.minimum),self.with_curly_braces,str(self.decimals)))
        def __repr__(self):
            return repr(str(self))

    lvar = []
    for d in ld:
        obj = O()
        obj.name_of_var = d['name_of_var']
        obj.maximum = d['maximum']
        obj.value = d['value']
        obj.minimum = d['minimum']
        obj.with_curly_braces = d['with_curly_braces']
        obj.decimals = d['decimals']
        #setattr(obj, key, d[key])
        lvar.append(obj)

    eq1 = u'the minimum speed is: {=pow(1.5,{n1}-1+0.5*(({n1}-1.1)/abs({n1}-1.1)-1))*sqrt({q1}*{sigma}*(sqrt({a1}*{a1}+{b1}*{b1})-{b1})/{m1}/8.854)} m/s'.encode('utf-8')
    eq2 = u'the minimum speed is: {=pow(1.5,{n1}-2+0.5*(({n1}-2.1)/abs({n1}-2.1)-1))*sqrt({q1}*{sigma}*(sqrt({a1}*{a1}+{b1}*{b1})-{b1})/{m1}/8.854)} m/s'.encode('utf-8')

    print calculate_moodle_exprs(eq2, lvar)
    print "\n************\n"
    print calculate_moodle_exprs(eq1, lvar)

    raise SystemExit
